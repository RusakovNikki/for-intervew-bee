import { useEffect, useState } from 'react';
import Loader from './components/Loader/Loader';
import { MyContext } from './context.js'
import './App.css';
import Table from './components/Table/Table';

function App() {
  let [isLoading, setLoading] = useState(true)
  let [data, setdata] = useState([])
  let [allSelectPersons, setSelect] = useState([])
  let [countAdd, setCountAdd] = useState(0)
  let [countDell, setCountDell] = useState(0)
  const url = 'https://jsonplaceholder.typicode.com/users'

  const requestData = async (url) => {
    const request = await fetch(url)
    const data = await request.json()

    setLoading(false)
    setdata(data)
  }
  function deleteElem(index) {
    console.log(index)
    setSelect(elem => elem.filter((qqq, indexI) => indexI !== index))
    setCountDell(prev => prev + 1)
  }

  function setHuman(item, e) {
    if (!allSelectPersons.filter(elem => JSON.stringify(elem) === JSON.stringify(item)).length) {
      setSelect(prev => [...prev, item])
      setCountAdd(prev => prev + 1)
      e.target.setAttribute('disabled', 'disabled')
    }
  }
  console.log(allSelectPersons);
  useEffect(() => {
    requestData(url)
  }, [])

  return (
    <div className="App">
      <div className="App">
        <MyContext.Provider value={{ data, setHuman, allSelectPersons, deleteElem, countAdd, countDell }}>
          {
            isLoading ?
              <Loader /> :
              <Table />
          }
        </MyContext.Provider>
      </div>
    </div>
  );
}

export default App;
