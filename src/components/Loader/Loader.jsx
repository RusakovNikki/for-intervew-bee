import React from 'react'
import loader from './spinner.svg'

const Loader = () => {
  return (
      <img src={loader} alt="loader" className='loader'/>
  )
}

export default Loader
