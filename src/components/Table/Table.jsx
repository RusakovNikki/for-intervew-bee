import React from 'react'
import { useState } from 'react'
import { useContext } from 'react'
import { MyContext } from '../../context'
import './Table.css'

const Table = () => {
    let [color, setColor] = useState('')
    let { data, setHuman, allSelectPersons, deleteElem, countDell, countAdd } = useContext(MyContext)
    console.log(data)
    return (
        <div className={'allContainer'}>
            <div className='secondElem'>
                {allSelectPersons.length ?
                    allSelectPersons.map((elem, index) => {
                        return (
                            <div key={elem.id + elem.name} className={'pasport'}>
                                <div>{elem.name}</div>
                                <div>{elem.phone}</div>
                                <div>{elem.website}</div>
                                <div>{elem.email}</div>
                                <div>{elem.company.name}</div>
                                <button onClick={() => deleteElem(index)}>Удалить из списка</button>
                            </div>
                        )
                    })
                    : <div>Пусто</div>}

            </div>
            {<div>добавлено {countAdd}</div>}
            {<div>Удалено {countDell}</div>}
            <div className='container'>

                {data && data.map((elem) => {
                    return <div className={`elemName`} 
                                onClick={(e) => {setHuman(elem, e)}} 
                                key={elem.id}>{elem.name}</div>
                })}
            </div>
        </div>
    )
}

export default Table
